## Exercice 3

# Objectif
L'objectif de cet exercice est de modifier le code de l'exercice précédent pour qu'il suive les bonnes pratiques de programmation (PEP8/PEP20)

# Réalisation
Pour modifier le code, on utilise les packages *black* et *pylint*, que l'on installe de la manière suivante :

    'pip install black'
    'pip install pylint'

On utilise tout d'abord l'outil *black* pour améliorer la mise en forme du code (sauts de ligne, indentations).
'black ex3.py'

L'outil *pylint* attribut une note au programme en fonction de l'application des bonnes pratiques de programmation. Il fournit également des commentaires indiquant les éléments ne respectant pas les bonnes pratiques.

    'pylint ex3.py'

On a donc ajouter une entête au programme et des commentaires dans la classe SimpleComplexCalculator.

# Notes
La note attribuée par *pylint* est de 8/10. En effet, les noms des attributs de la classe ne respectent pas la convention "snake-case", mais modifier le nom des attributs ne rend pas le code plus lisible.

